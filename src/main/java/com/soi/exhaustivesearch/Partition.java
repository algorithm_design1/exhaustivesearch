/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.exhaustivesearch;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author user
 */
public class Partition {

    static ArrayList arr = new ArrayList();

    static boolean isSubsetSum(ArrayList arr, int n, int sum) {

        if (sum == 0) {
            return true;
        }
        if (n == 0 && sum != 0) {
            return false;
        }

        if ((int) arr.get(n - 1) > sum) {
            return isSubsetSum(arr, n - 1, sum);
        }
        return isSubsetSum(arr, n - 1, sum)
                || isSubsetSum(arr, n - 1, sum - (int) arr.get(n - 1));
    }

    static boolean findPartition(ArrayList arr, int n) {
        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum += (int) arr.get(i);
        }
        if (sum % 2 != 0) {
            return false;
        }
        return isSubsetSum(arr, n, sum);
    }

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int n = kb.nextInt();
        for (int i = 0; i < n; i++) {
            arr.add(kb.nextInt());
        }
        if (findPartition(arr, n) == true) {
            System.out.println("Can be divided into two "
                    + "subsets of equal sum");
        } else {
            System.out.println(
                    "Can not be divided into "
                    + "two subsets of equal sum");
        }
    }

}
